Title: Hello Word!
Date: 2020-02-13
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

Hola Mundo en español.

**Bienvenido.**

Soy Deybi Morales. Economista que reside en Nicaragua un país que queda en el centro del mapa de Centroamérica.

Soy un apasionado por las computadoras. Aprendiendo cosas nuevas y aplicándola a la economía. Pero la mejor manera de aprender es enseñando.

He creado este blog para aprender enseñando. 


**Aquí compartiré:**

1. Tutoriales sobre R, Python, Excel, PowerBI, etc

2. Indicadores Económicos y Financieros

3. Noticias Económicas relevantes

4. Publicaciones relevantes

**Las mejores manera de apoyarme son:**

1. Seguirme y compartirme en redes sociales.

2. Enviarme sus comentarios y sugerencias.

3. Solicitar temáticas nuevas para escribir.

4. Contratar implementaciones informáticas a nivel académico o empresarial de R, Python, PowerBI, etc.

5. Solicitar capacitaciones.

6. Adquirir los productos que publicamos.

*Reitero la bienvenida*. 

*¡Que encuentres lo que buscas!*

*¡Regresa pronto!*

Puede contactarme al correo: morales.economia@gmail.com